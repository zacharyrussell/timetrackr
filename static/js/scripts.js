var timers = [];
$(document).ready(function() {
  jQuery.getJSON('/timers', function (data) {
    timers = data;
    for(i in timers) {
      if( timers[i].status === 'running') {
        $(".timers").append('<li id="timer-' + i + '" class="timer">' + data[i].project + ' ' + '<span>' + convertToTimer(data[i].totalTime) + '</span>' + ' <button type="button" class=" btn btn-success" status="playing" id=' + i + '><i class="fa fa-pause"></i></button>' + '</li>');
        startTimer(i)
      } else {
        $(".timers").append('<li id="timer-' + i + '" class="timer">' + data[i].project + ' ' + '<span>' + convertToTimer(data[i].totalTime) + '</span>' + ' <button type="button" class=" btn btn-success" id=' + i + '><i class="fa fa-play"></i></button>' + '</li>');

      }
    }
});
});
function convertToTimer(milliseconds) {
  hours = Math.floor((milliseconds/1000/60/60) << 0),
  min = Math.floor((milliseconds/1000/60) << 0),
  sec = Math.floor((milliseconds/1000) % 60);

  minsFormatted = ((min>0) ? ((min>9) ? min : '0'+min) : "00");
  secsFormatted = ((sec>0) ? ((sec>9) ? sec : '0'+sec) : "00");
  hoursFormatted = ((hours>0) ? ((hours>9) ? hours : '0'+hours) : "00");
  return(hoursFormatted + ":" + minsFormatted + ":" + secsFormatted);
}
$('.timers').on('click', 'li>button', function() {
  id = $(this).attr('id');
  if($(this).attr('status') === 'paused' || $(this).attr('status') === undefined) {
    startTimer(id);
    $(this).attr('status', 'playing');
    $(this).html('<i class="fa fa-pause"></i>');
  } else if($(this).attr('status') === 'playing') {
    stopTimer(id);
    $(this).attr('status', 'paused');
    $(this).html('<i class="fa fa-play"></i>');
  }


})
function startTimer(id) {
  timers[id].status = 'running';
   var interval = setInterval( function() {
     timers[id].totalTime += 1000;
     updateID = "#timer-" + id.toString() + '>span';
     $(updateID).html(convertToTimer(timers[id].totalTime));
   }, 1000)
   timers[id]["intervalId"] = interval;
   updateTimer(id);
   return interval;

}
function stopTimer(id) {
  clearInterval(timers[id].intervalId);
  timers[id].status = 'paused';
  updateTimer(id);

}
function updateTimer(id) {
  uuid = timers[id].id;
  $.ajax({
      url: '/timers/' + uuid,
      contentType: "application/json",
      type: 'PUT',
      data: JSON.stringify(timers[id]),
      success: function(data) {
        console.log(data);
      }
    });
}
