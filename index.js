var express = require('express');
var app = express();
var nodePort = process.argv[2]
var config = require('./configs/configs.js');
var timers = require('./lib/timers')
var routes = require('./lib/routes')(app, function(err) {
  //if (err) throw err;
  app.listen(nodePort, function() {
      console.log('app is listening at port ' + nodePort);
  });
});
