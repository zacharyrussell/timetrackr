var timers = require('./timers');
var bodyParser = require('body-parser');
var path = require('path');
var express = require('express');
var Validator = require('jsonschema').Validator;
var v = new Validator();

module.exports = function(app, callback) {
    app.use(bodyParser.json());
    app.use('/', express.static(path.join(__dirname, '../static')))
    app.get('/timers', function(req, res) {
        var params = req.query;
        timers.getTimers(params, function(err, response) {
          res.setHeader('Content-Type', 'application/json');
          res.send(JSON.stringify(response, null, 2));
        });
    });
    app.get('/timers/:id', function(req, res) {
        timers.getTimer(req.params["id"], function(err, response) {
          res.setHeader('Content-Type', 'application/json');
          res.send(JSON.stringify(response, null, 2));
        });
    });
    app.post('/timers', function(req, res) {
        timers.createTimer(req.body["project"], req.body["description"], function(err, response) {
            res.send(JSON.stringify(response, null, 2));
        });

    });
    app.put('/timers/:id', function(req, res) {
        var schema = {
            'id': '/timerUpdate',
            'type': 'object',
            'properties': {
                'status': {
                    'type': 'string'
                },
                'project': {
                    'type': 'string'
                },
                'description': {
                    'type': 'string'
                },
                'totalTime': {
                    'type': 'number'
                }
            },
            'required': ['totalTime'],
            'additionalProperties': true 
        };
        var validateJSON = v.validate(req.body, schema);
        if (validateJSON.errors.length > 0) {
             res.setHeader('Content-Type', 'application/json');
             res.send(JSON.stringify(validateJSON.errors));
        } else {
            timers.updateTimer(req.params["id"], req.body, function (err, response) {
                res.send(JSON.stringify(response, null, 2));
            });
        };

    });
    app.delete('/timers/:id', function(req, res) {
        timers.deleteTimer(req.params["id"], function(err, response) {
          res.setHeader('Content-Type', 'application/json');
          res.send(JSON.stringify(response, null, 2));
        })
    });
    console.log('about to initalize timers');
    timers.init(callback);
}
