var uuidV4 = require('uuid/v4');
var db = require('./db/rethinkdb.js');


module.exports = {
    getTimers: getTimers,
    getTimer: getTimer,
    createTimer: createTimer,
    updateTimer: updateTimer,
    deleteTimer: deleteTimer,
    init: db.init,

}
// Create a new timer
function createTimer( project, description, callback) {
    //build timer object
    var id = uuidV4();
    var timerInfo = {
        id: id,
        status: "running",
        project: project,
        description: description,
        created: Date.now(),
        lastUpdated: Date.now(),
        totalTime: 0,
    }

    return db.addTimer(id, timerInfo, callback);
}

// Update existing Timer
function updateTimer(id, obj, callback) {
    return db.updateTimer(id, obj, callback);
}

// Delete Timer
function deleteTimer(id, callback) {
   return db.deleteTimer(id, callback);
}

// Get Timers
function getTimers(params, callback) {
    return db.getTimers(params, callback);
}

// Get Timer
function getTimer(id, callback) {
    return db.getTimer(id, callback);
}

