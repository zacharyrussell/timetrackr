var memorystore = {} = module.exports;

memorystore.init = function(callback) {
  timers = {};
  console.log('in memstore init');
  return process.nextTick(callback);
}
memorystore.addTimer = function(id, object, callback) {
  timers[id] = object;
  return process.nextTick(callback, null, object);
}

// module.exports = {
//     addTimer: addTimer,
//     getTimers: getTimers,
//     getTimer: getTimer,
//     updateTimer: updateTimer,
//     deleteTimer: deleteTimer,
// }
var timers;
function addTimer(id, object) {
    timers[id] = object;
    return id;
}

function updateTimer(id, object) {
    timers[id] = object;
    return[id];
}

function deleteTimer(id) {
    delete timers[id];
    return('deleted ' + id);
}

function getTimers()  {
    return timers;
    /** Return an array ***/
}

function getTimer(id) {
    return timers[id];
}
